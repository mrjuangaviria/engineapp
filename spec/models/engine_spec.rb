require 'rails_helper'

RSpec.describe Engine, type: :model do
  it 'has a valid factory' do
    expect(build(:engine)).to be_valid
  end

  subject { build(:engine) }

  describe 'ActiveModel validations' do
    it { expect(subject).to validate_presence_of(:name) }
    it { expect(subject).to validate_presence_of(:description) }
    it { expect(subject).to validate_presence_of(:cc) }
    it { expect(subject).to validate_presence_of(:brand) }
    it { expect(subject).to validate_presence_of(:image) }
  end
end
