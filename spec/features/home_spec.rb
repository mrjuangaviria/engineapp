# require 'rails_helper'

# describe "Navigation Page Feature", type: :feature do

#   before(:each)  do
#     Capybara.current_driver = :selenium
#     Capybara.default_max_wait_time = 6
#     Capybara.save_path = "./spec/fail"
#   end

#   it "Visit Home Page" do
#     visit '/'
#     page.has_text?('Engine App')
#   end

#   it "Visit Motorcycle Page and Create Motorcycle" do
#     visit '/'
#     click_link 'View Motorcycles'
#     page.has_text?('The best Motorcycles')
#   end

#   it "Create Motorcycle" do
#     visit '/'
#     click_link 'View Motorcycles'
#     click_link 'Create Motorcycle Engine'
#     page.has_text?('Add a new engine to our awesome collection.')
#     fill_in 'name', with: 'GsxR600'
#     fill_in 'description', with: 'Fast'
#     fill_in 'cc', with: 600
#     fill_in 'brand', with: 'Susuki'
#     fill_in 'image', with: 'http://suzuki.com.pe/motos/wp-content/uploads/2016/11/GSX_R600L7_YSF_D-azul-1.png'
#     click_button('Create Motorcycle')
#     page.has_text?('GsxR600')
#   end
# end
