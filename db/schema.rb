# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_08_024459) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "engines", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.integer "cc", null: false
    t.string "brand", null: false
    t.string "image", default: "https://techcrunch.com/wp-content/uploads/2019/06/BMW-Motorrad-Vision-DC-Roadster-12.jpg?w=730&crop=1"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
