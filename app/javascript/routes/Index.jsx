import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "../components/Home";
import Motorcycles from "../components/Motorcycles";
import Motorcycle from "../components/Motorcycle";
import NewMotorcycle from "../components/NewMotorcycle";

export default (
  <Router>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/engines" exact component={Motorcycles} />
      <Route path="/engine/:id" exact component={Motorcycle} />
      <Route path="/engine" exact component={NewMotorcycle} />
    </Switch>
  </Router>
);
