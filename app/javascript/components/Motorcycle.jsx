import React from "react";
import { Link } from "react-router-dom";

class Motorcycle extends React.Component {
  constructor(props) {
    super(props);
    this.state = { motorcycle: { description: "" } };

    this.addHtmlEntities = this.addHtmlEntities.bind(this);
    this.deleteMotorcyle = this.deleteMotorcyle.bind(this);
  }

  componentDidMount() {
    const {
      match: {
        params: { id }
      }
    } = this.props;

    const url = `/api/v1/show/${id}`;

    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(response => this.setState({ motorcycle: response }))
      .catch(() => this.props.history.push("/engines"));
  }

  addHtmlEntities(str) {
    return String(str)
      .replace(/&lt;/g, "<")
      .replace(/&gt;/g, ">");
  }

  deleteMotorcyle() {
    const {
      match: {
        params: { id }
      }
    } = this.props;
    const url = `/api/v1/destroy/${id}`;
    const token = document.querySelector('meta[name="csrf-token"]').content;

    fetch(url, {
      method: "DELETE",
      headers: {
        "X-CSRF-Token": token,
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(() => this.props.history.push("/engines"))
      .catch(error => console.log(error.message));
  }

  render() {
    const { motorcycle } = this.state;

    return (
      <div className="">
        <div className="hero position-relative d-flex align-items-center justify-content-center">
          <img
            src={motorcycle.image}
            alt={`${motorcycle.name} image`}
            className="img-fluid position-absolute"
          />
          <div className="overlay bg-dark position-absolute" />
          <h1 className="display-4 position-relative text-white">
            {motorcycle.name}
          </h1>
        </div>
        <div className="container py-5">
          <div className="row">
            <div className="col-sm-4 col-lg-3">
              <ul className="list-group">
                <h5 className="mb-2">Description</h5>
                <div
                dangerouslySetInnerHTML={{
                  __html: `${motorcycle.description}`
                }}
              />
              </ul>
            </div>
            <div className="col-sm-4 col-lg-3">
              <h5 className="mb-2">C.C. Engine</h5>
              <div
                dangerouslySetInnerHTML={{
                  __html: `${motorcycle.cc}`
                }}
              />
            </div>
            <div className="col-sm-4 col-lg-3">
              <h5 className="mb-2">Engine Brand</h5>
              <div
                dangerouslySetInnerHTML={{
                  __html: `${motorcycle.brand}`
                }}
              />
            </div>
            <div className="col-sm-12 col-lg-2">
              <button type="button" className="btn btn-danger" onClick={this.deleteMotorcyle}>
                Delete Motorcycle
              </button>
            </div>
          </div>
          <Link to="/engines" className="btn btn-link">
            Back to motorcycles
          </Link>
        </div>
      </div>
    );
  }
}

export default Motorcycle;