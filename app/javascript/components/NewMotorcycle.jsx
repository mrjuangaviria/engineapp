import React from "react";
import { Link } from "react-router-dom";

class NewMotorcycle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      description: "",
      cc: 0,
      brand: "",
      image: ""
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  onSubmit(event) {
    event.preventDefault();
    const url = "/api/v1/engines/create";
    const { name, description, cc, brand, image } = this.state;

    if (name.length == 0 || description.length == 0 || cc.length == 0 || brand.length == 0)
      return;

    const body = {
      name,
      description,
      cc,
      brand,
      image
    };

    const token = document.querySelector('meta[name="csrf-token"]').content;
    fetch(url, {
      method: "POST",
      headers: {
        "X-CSRF-Token": token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(response => this.props.history.push(`/engine/${response.id}`))
      .catch(error => console.log(error.message));
  }

  render() {
    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-sm-12 col-lg-6 offset-lg-3">
            <h1 className="font-weight-normal mb-5">
              Add a new engine to our awesome collection.
            </h1>
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="motorcycleName">Motorcycle name</label>
                <input
                  type="text"
                  name="name"
                  id="motorcycleName"
                  className="form-control"
                  required
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="motorcycleDescription">Description</label>
                <input
                  type="text"
                  name="description"
                  id="motorcycleDescription"
                  className="form-control"
                  required
                  onChange={this.onChange}
                />
              </div>
              <label htmlFor="cc">CC</label>
              <input
                className="form-control"
                id="cc"
                name="cc"
                rows="5"
                required
                onChange={this.onChange}
              />
              <label htmlFor="brand">Brand</label>
              <input
                className="form-control"
                id="brand"
                name="brand"
                rows="5"
                required
                onChange={this.onChange}
              />
              <div className="form-group">
                <label htmlFor="mImage">Motorcycle image</label>
                <input
                  type="text"
                  name="image"
                  id="mImage"
                  className="form-control"
                  onChange={this.onChange}
                />
              </div>
              <button type="submit" className="btn custom-button mt-3">
                Create Motorcycle
              </button>
              <Link to="/engines" className="btn btn-link mt-3">
                Back to Motorcycles
              </Link>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default NewMotorcycle;