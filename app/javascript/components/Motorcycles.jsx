import React from "react";
import { Link } from "react-router-dom";

class Motorcycles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      motorcycles: []
    };
  }

  componentDidMount() {
    console.log('componentDidMount');
    const url = "/api/v1/engines/index";
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(response => this.setState({ motorcycles: response }))
      .catch(() => this.props.history.push("/"));
  }

  render() {
    const { motorcycles } = this.state;
    const allMotorcycles = motorcycles.map((motorcycle, index) => (
      <div key={index} className="col-md-6 col-lg-4">
        <div className="card mb-4">
          <img
            src={motorcycle.image}
            className="card-img-top card-image"
            alt={`${motorcycle.name} image`}
          />
          <div className="card-body">
            <h5 className="card-title">{motorcycle.name}</h5>
            <Link to={`/engine/${motorcycle.id}`} className="btn custom-button">
              View motorcycle
            </Link>
          </div>
        </div>
      </div>
    ));
    const noMotorcycle = (
      <div className="vw-100 vh-50 d-flex align-items-center justify-content-center">
        <h4>
          No motorcycles yet. Why not <Link to="/new_motorcycle">create one</Link>
        </h4>
      </div>
    );

  return (
      <>
        <section className="jumbotron jumbotron-fluid text-center">
          <div className="container py-5">
            <h1 className="display-4">The best Motorcycles</h1>
            <p className="lead text-muted">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat.
            </p>
          </div>
        </section>
        <div className="py-5">
          <main className="container">
            <div className="text-right mb-3">
              <Link to="/engine" className="btn custom-button">
                Create Motorcycle Engine
              </Link>
            </div>
            <div className="row">
              {motorcycles.length > 0 ? allMotorcycles : noMotorcycle}
            </div>
            <Link to="/" className="btn btn-link">
              Home
            </Link>
          </main>
        </div>
      </>
    );
  }

}
export default Motorcycles;