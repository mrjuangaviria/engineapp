import React from "react";
import { Link } from "react-router-dom";

export default () => (
  <div className="vw-100 vh-100 primary-color d-flex align-items-center justify-content-center">
    <div className="container main-color">
      <div className="jumbotron jumbotron-fluid bg-transparent">
        <div className="container secondary-color">
          <h1 className="display-4">Engine App</h1>
          <p className="lead">
            List of the best Motorcycles on the market.
          </p>
          <hr className="my-4" />
          <Link
            to="/engines"
            className="btn btn-lg custom-button"
            role="button"
          >
            View Motorcycles
          </Link>
        </div>
      </div>
    </div>
  </div>
);
