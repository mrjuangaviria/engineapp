class Engine < ApplicationRecord
  validates :name, presence: true
  validates :description, presence: true
  validates :cc, presence: true
  validates :brand, presence: true
  validates :image, presence: true
end
